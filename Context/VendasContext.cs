using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models; 


namespace tech_test_payment_api.Context
{
    public class OrganizadorContext : DbContext
    {
        public OrganizadorContext(DbContextOptions<OrganizadorContext> options) : base(options)
        {
            
        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedors { get; set; }
        public DbSet<Item> Itens { get; set; }
    }
}