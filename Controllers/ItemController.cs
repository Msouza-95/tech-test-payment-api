using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly OrganizadorContext _context; 


        public ItemController(OrganizadorContext context){
            _context = context; 
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {

            var item = _context.Itens.Find(id);

            
            if(item == null){
                return NotFound();
            }

            
            return Ok(item);
        }

        [HttpGet("All")]
        public IActionResult Show()
        {
            
            var itens = _context.Itens.ToArray();
            return Ok(itens);
        }

         [HttpPost("register")]
        public IActionResult Create(Item item)
        {
            if (item.Preco == null)
                return BadRequest(new { Erro = "O item não pode ser cadastrado sem preço" });

            
            _context.Add(item);
            _context.SaveChanges();

            return CreatedAtAction(nameof(Read), new { id = item.Id }, item);
        }

    }
}