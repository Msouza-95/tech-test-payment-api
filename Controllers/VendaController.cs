using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models; 
using tech_test_payment_api.Context;



namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {

         private readonly OrganizadorContext _context; 


        public VendaController(OrganizadorContext context){
            _context = context; 
        }
        



        [HttpPost("register")]
        public  IActionResult Create(RequestVenda req)
        {


            var findVendedor = _context.Vendedors.Find(req.VendedorId);

            if(findVendedor == null){
                 return NotFound("id do vendedor invalido");
            }

            var itensId = req.ItensId;

            if(itensId.Count <= 0){
                return NotFound(" Precisa ter pelo menos 1 item no pedido");
            }

            // verificar se todos os id de itens são validos
            foreach (var id in itensId)
            {
                var item = _context.Itens.Find(id);

                if(item == null){
                    return NotFound($"o id: {id} não é de item valido");
                }
            }

            

                // salvar as vendas

            foreach (var id in itensId)

            {
                var novaVenda = new Venda();
                novaVenda.Data = req.Data;
                novaVenda.VendedorId = req.VendedorId;
                novaVenda.Status = EnumStatusVendas.AguardandoPagamento;
                novaVenda.IdentificadorPedido = req.IdentificadorPedido;
                novaVenda.ItensId = id;
                 _context.Add(novaVenda);
                 _context.SaveChanges();

            }



            return Ok(req);
        }

        
        
        
        [HttpGet("All")]
        public IActionResult Show()
        {
            
            var venda = _context.Vendas.ToArray();
            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {

            var venda = _context.Vendas.Find(id);

            
            if(venda == null){
                return NotFound();
            }

            
            return Ok(venda);
        }

        [HttpGet("Update/Status{id}")]
        public IActionResult AtulizarVenda(int id, EnumStatusVendas status)
        {

            var venda = _context.Vendas.Find(id);

                if(venda == null){
                return NotFound("Venda não encontrada");
            }


            if(status == EnumStatusVendas.PagamentoAprovado){

                if(venda.Status != EnumStatusVendas.AguardandoPagamento){
                    return NotFound($"O status atual da venda é{venda.Status} não pode ser mudado para {status}"); 
                }

                venda.Status = status;
                _context.Update(venda);
                _context.SaveChanges();

                return Ok(venda);

            }

            if(status == EnumStatusVendas.Cancelada){

                if(venda.Status != EnumStatusVendas.AguardandoPagamento || venda.Status != EnumStatusVendas.PagamentoAprovado ){
                    return NotFound($"O status atual da venda é{venda.Status} não pode ser mudado para {status}"); 
                }


                venda.Status = status;

                _context.Update(venda);
                _context.SaveChanges();

                return Ok(venda);

            }

            if(status == EnumStatusVendas.EnviadoParaTransportadora){

                if(venda.Status != EnumStatusVendas.PagamentoAprovado){
                    return NotFound($"O status atual da venda é{venda.Status} não pode ser mudado para {status}"); 
                }

                venda.Status = status;

                _context.Update(venda);
                _context.SaveChanges();

                return Ok(venda);

            }


            if(status == EnumStatusVendas.Entregue){

                if(venda.Status != EnumStatusVendas.EnviadoParaTransportadora){
                    return NotFound($"O status atual da venda é{venda.Status} não pode ser mudado para {status}"); 
                }

                venda.Status = status;

                _context.Update(venda);
                _context.SaveChanges();

                return Ok(venda);

            }

            return Ok(venda);
        }


    }
}