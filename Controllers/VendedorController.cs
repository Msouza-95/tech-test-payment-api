using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly OrganizadorContext _context; 


        public VendedorController(OrganizadorContext context){
            _context = context; 
        }

        [HttpGet("{id}")]
        public IActionResult Read(int id)
        {

            var vendedor = _context.Vendedors.Find(id);

            
            if(vendedor == null){
                return NotFound();
            }

            
            return Ok(vendedor);
        }

        [HttpGet("All")]
        public IActionResult Show()
        {
            
            var vendedors = _context.Vendedors.ToArray();
            return Ok(vendedors);
        }

         [HttpPost("register")]
        public IActionResult Create(Vendedor vendedor)
        {
            if (vendedor.Nome == null)
                return BadRequest(new { Erro = "Um vendedor precisa de um nome" });

            
            _context.Add(vendedor);
            _context.SaveChanges();

            return CreatedAtAction(nameof(Read), new { id = vendedor.Id }, vendedor);
        }

    }
}