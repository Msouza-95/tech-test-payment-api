using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public struct RequestVenda
    {
        public int VendedorId  { get; set; }
        public List<int> ItensId{ get; set; }
        public DateTime Data { get; set; }
        public string IdentificadorPedido { get; set; }
       
        
    }
}