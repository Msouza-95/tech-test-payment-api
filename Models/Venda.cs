using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {


        
        public int? ID  { get; set; }
        public int ItensId{ get; set; }
        public int  VendedorId { get; set; }
        public DateTime Data { get; set; }
        public string IdentificadorPedido { get; set; }
        public EnumStatusVendas Status { get; set; }
        

    }
}